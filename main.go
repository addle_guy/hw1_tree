package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strconv"
)

const (
	branch     = "├───"
	lastBranch = "└───"
	trait      = "│\t"
	tab        = "\t"
	newLine    = "\n"
)

type Node interface {
	fmt.Stringer
	IsDir() bool
}

type Dir struct {
	name  string
	nodes []Node
}

type File struct {
	name string
	size int64
}

func (d Dir) String() string {
	return d.name
}

func (d Dir) IsDir() bool {
	return true
}

func (f File) String() string {
	if f.size == 0 {
		return f.name + " (empty)"
	}
	return f.name + " (" + strconv.FormatInt(f.size, 10) + "b)"
}

func (f File) IsDir() bool {
	return false
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}

func dirTree(out io.Writer, path string, isPrintFiles bool) error {
	err, tree := buildTree(path, isPrintFiles)
	if err != nil {
		return err
	}
	printTree(out, tree, "")
	return nil
}

func buildTree(path string, isPrintFiles bool) (error, []Node) {

	root, err := os.Open(path)
	if err != nil {
		return err, nil
	}
	defer root.Close()

	files, err := root.Readdir(0)
	if err != nil {
		return err, nil
	}

	// Sort filenames
	sort.Slice(files, func(i, j int) bool {
		return files[i].Name() < files[j].Name()
	})

	var tree []Node
	for _, file := range files {

		if !(file.IsDir() || isPrintFiles) {
			continue
		}

		var node Node
		if file.IsDir() {
			err, subnodes := buildTree(filepath.Join(path, file.Name()), isPrintFiles)
			if err != nil {
				return err, nil
			}
			node = Dir{file.Name(), subnodes}
		}
		if !(file.IsDir()) {
			node = File{file.Name(), file.Size()}
		}

		tree = append(tree, node)
	}
	return nil, tree
}

func printTree(out io.Writer, tree []Node, tail string) {

	for i := 0; i < len(tree); i++ {
		if i == len(tree)-1 {
			fmt.Fprintf(out, "%s%s%s\n", tail, lastBranch, tree[i].String())
		} else {
			fmt.Fprintf(out, "%s%s%s\n", tail, branch, tree[i].String())
		}

		if tree[i].IsDir() {
			var dir = tree[i].(Dir)
			if i == len(tree)-1 {
				printTree(out, dir.nodes, tail+tab)
			} else {
				printTree(out, dir.nodes, tail+trait)
			}
		}
	}

	return
}
